---
layout: post
title: "Surat Kabar Yearn Finance #58"
categories: [Newsletters]
image:
src: ./cover.png
width: 576
height: 288
author: Yearn
date: '2022-03-08'
translator: empeje
---

### Akhir Pekan Maret 8, 2022

![](./image1.png?w=1456&h=733)

Selamat datang di edisi ke-58 dari Surat Kabar Yearn Finance. Tujuan kami untuk surat kabar ini adalah untuk menjaga Yearn dan komunitas kripto secara umum mendapatkan informasi dan kabar terbaru, termasuk peluncuran produk, perubahan governans, dan kabar terbaru dari ekosistem. JIka kamu tertarik untuk mempelajari lebih lanjut tentang Yearn Finance, ikuti kanal resmi [Twitter](https://twitter.com/iearnfinance) dan [Medium](https://medium.com/iearn).


## Rangkuman

- Yearn di Arbitrum
- Meninjau Keamanan Yearn
- Mengklarifikasi Merger 2020
- Masuk ke Pengukuran Yearn
- Yearn Berpartner dengan Tenderly
- Keterlibatan Andre Cronje pada DeFi
- Lemari Besi Yearn
- Berita dari Ekosistem

# Yearn di Arbitrum

![](./image2.jpg?w=1000&h=1000)

Yearn baru saja hadir di Arbitrum, dukungan Ethereum L2 pertama. Ini juga peluncuran kedua untuk deployment mainnet non-Ethereum setelah Fantom.

Arbitrum adalah L2 terbesar Ethereum, dengan penurunan biaya gas sebesar 10x dan dukungan untuk penarikan dan deposit di bursa top. Untuk memulai, Yearn akan hadir di Arbitrum dengan lemari besi Curve triCrypto yang terdiri dari ETH, USDT, dan WBTC.

Untuk masuk lemari besi ini, kamu akan perlu menambahkan Arbitrum ke Metamask, kemudian memakai jembatan melalui Synapse, Hop, atau jembatan resmi dari Arbitrum, kemudian menambahkan likuiditas di triCrypto pada [curve.fi](https://arbitrum.curve.fi/), dan akhirnya, deposit ke [yearn.finance/vaults](http://yearn.finance/vaults).

Kami mengundangmu untuk ikut serta, memberikan masukan, dan berpartisipasi di komunitas Yearn dengan bergabung dengan kami di [Discord](https://discord.gg/8rF374XkXy), [Twitter](http://twitter.com/iearnfinance), atau [Github](http://github.com/yearn). Ayo membangun bersama kami.

# Meninjau Keamanan Yearn

![](./image3.jpg?w=1000&h=563)

Di Yearn, kami memperhatikan keamanan dengan sangat serius dan berharap untuk dapat memberikan hasil yang disesuaikan dengan resiko terbaik pada DeFi.

Terimakasih kepada DefiSafety, suatu tim auditor yang independen, kami mendapatkan skor 93%. Disamping dari langkah-langkah keamanan yang sudah diambil dan dokumentasi yang menyeluruh, Yearn juga melaksanakan verifikasi formal, yang mana menjadi tahap terpisah dari proses audit.

Baca audit lengkapnya [di sini](https://www.defisafety.com/pqrs/354).

# Mengklarifikasi Merger 2020

Pada akhir 2020 Yearn Finance mengumumkan beberapa "merger" dengan tujuan untuk membawah proyek DeFi terkemuka bersama, membuat suatu ekosistem yang fokus pada infrastruktur kritikal: AMM (SushiSwap), Pinjaman (C.R.E.A.M. Finance), dukungan institusional (Akropolis), dan manajemen hasil (Pickle Finance).

Terlepas dari keberhasilan dari berbagai arah, kami tidak pernah dapat bisa menjadi entitas yang tergabung jadi satu. Kedepannya, setiap protokol akan beroperasi secara independen sementara tetap menjadi kolaborator kuat dan menjadi teman baik dari Yearn dan satu sama lain.

Untuk itu, Iron Bank akan beroperasi sebagai proyek yang independen dari Yearn.


Baca artikel lengkapnya [di sini](https://medium.com/iearn/clarifying-2020-mergers-an-independent-iron-bank-a6f8f3f4c25e).

# Masuk ke Pengukuran Yearn

![](./image4.png?w=1400&h=625)

Salah satu etos utama dari kripto adalah untuk setiap individu melakukan risetnya masing-masing, di sini di Yearn, kami menggaris bawahi bahwa ukuran transparensi adalah salah satu kekuatan kami dalam membantu pengguna untuk berhati-hati dalam berinteraksi dengan protokol.

Terdapat tiga situs yang biasa kami rekomendasikan yaitu [The Vaults at Yearn](https://vaults.yearn.finance/), [Yearn Vision](https://yearn.vision/), dan [Yearn Watch](https://yearn.watch/).

The Vaults at Yearn adalah yang paling sederhana, memberikan rangkuman singkat dari strategi lemari besi dan juga jenis-jenis lemari besi yang ada pada Yearn. Yearn Vision adalah memberikan dasbor yang paling lengkap yang dapat memberikan informasi dan kesehatan dari lemari-lemari besi, total nilai terkunci, dan beberapa analisis lain. Terakhir, Yearn Watch adalah alat yang dipakai untuk para strategis untuk memahami informasi tentang kesehatan dari strategi-strategi tertentu.

Cek artikel lengkap dari MarcoWorms dan Dark Ghosty [di sini](https://medium.com/iearn/diving-into-yearn-metrics-8c3fb0520927).

Kamu dapat mengakses lemari besi yang dicantumkan pada situs dan mulai mendapatkan keuntungan hari ini di [yearn.finance/#/vaults](https://yearn.finance/#/vaults).

# Yearn Berpartner dengan Tenderly

![](./image5.png?w=1400&h=670)

Berita sangat hangat datang dari penggalangan dana Seri B sebesar $40 juta dollar, plafon alat pengembangan Ethereum Tenderly membuka kerjasama resmi dengan kami Yearn.

Sebagaimana Yearn sudah menjadi suatu raksasa DeFi, ada lebih dari 300 kontrak pintar yang kami ciptakan hanya untuk lemari besi v2 misalnya. Ini hanya meliputi lapisan permukaan dari banyak sekali kontrak pintar yang telah kami tulis dengan lemari besi v1, infrastruktur keep3r, kontrak pembantu, dan juga pemasangan jaringan-sisi yang sudah dibuat.

Dengan banyaknya tim, juga jumlah kontrak-kontrak, dan juga kebutuhan untuk mengirimkan hasil lebih cepat, kami membutuhkan alat pengembangan yang paling baik dalam kelasnya. Tenderly telah memunginkan kami untuk mempercepat pemantauan & peringatan, debug, dan juga analisis insiden untuk infrastruktur protokol secara umum dan juga saat kita di ruang tempur.

Satu hal yang Yearn sangat bersemangat adalah tentang membawa kontributor baru dan menarik talenta. Dalam kolaborasi bersama tim Tenderly, kami memberikan akses 90-hari gratis ke Tenderly untuk anggota dari YFI Boarding School (suatu komunitas untuk strategis di masa depan). Kami percaya bahway ini dapat membantu anggota boarding school untuk mendapatkan wawasan yang lebih dalam untuk kode strategi, membantu mereka belajar lebih efisien, juga menunjukan kepada mereka kemampuan dari produk Tenderly.

Tenderly memberikan suatu plafon pengembang yang hulu-ke-hilir untuk tim dan perusahaan yang sedang mengembangkan inovasi produk blockchain. Pelajari lebih lanjut di [tenderly.co](https://tenderly.co/).

Kamu dapat membaca rangkuman lengkap dari kerjasama ini [di sini](https://medium.com/iearn/yearn-finance-partners-with-tenderly-to-supercharge-development-debugging-incident-analysis-6489260298a5).

# Keterlibatan Andre Cronje pada DeFi

Sebagaimana yang kamu sudah tahu, Andre Cronje, pendiri Yearn, sudah mengumumkan kepergiannya dari lingkungan kripto/DeFi. Meskipun begitu, kami ingin memberi tahu bahwa Andre sudah tidak terlibat lebih dari 1 tahun dan kami memiliki lebih dari 50 pekerja tetap dan lebih dari 40 kontributor tidak tetap di Yearn.

Pensiunnya Andre tidak memberikan dampak pada operasional hari-ke-hari Yearn, dan infrastruktur kita terus berjalan sebagai yang paling kokoh. Perubahan tokenomics kita yang akan segara datang dan beberapa YIP juga sudah di tempatnya untuk diluncurkan.


# Lemari Besi Yearn

Kami dapat belajar lebih lanjut deskripsi detail dari strategi-strategi lemari besi dari yVaults kita yang aktif [di sini](https://medium.com/yearn-state-of-the-vaults/the-vaults-at-yearn-9237905ffed3).


# Berita dari Ekosistem

[Solidly AMM baru dari Andre Cronje telah meluncurkan token di Fantom](https://solidly.exchange/)

[Jual-belikan pasangan YFI/DAI baru di Premia Finance pada Arbitrum dan Ethereum](https://twitter.com/PremiaFinance/status/1497313221123837959)

[Cek salah satu strategi koin stabil baru dalam kerjasama dengan Notional Finance](https://twitter.com/teddywoodward/status/1497229571799801865)

[Tempus Finance sekarang hadir di Fantom dengan integrasi Yearn hasil-tetap](https://twitter.com/TempusFinance/status/1495747382285377538)

[Jika kamu memiliki YFI atau WOOFY NFTs pada Fantom, cek PaintSwap atau pasarloka NFTKEY untuk suatu kejutan dari FakeWorms Studio](https://twitter.com/MarcoWorms/status/1497601119220076544)

[Baca impresi awal dari kontributor Yearn Kishvd tentang budaya DAO Yearn](https://kishvd.medium.com/my-first-impressions-of-being-a-contributor-at-yearn-e154743b9cd5)

[Dengarkan tentang kenapa Matt West, salah satu mantan strategis Yearn, yang sekarang mencalonkan sebagai anggota Kongres](https://twitter.com/DeFi_Dad/status/1496568281070776321?s=20&t=FA6P4ib_P1NZz_lmoXxvSw)

[Cek kuliah ETHDenver Tracheopteryx tentang bagaimana cara berkontribusi di DAO](https://youtu.be/anDAtWrhDnE)

[Pills Universe sudah meluncurkan karya seni baru yang melibatkan Yearn](https://twitter.com/pillsuniverse/status/1494343761022918658)

[Baca tentang kerjasama ShapeShift dengan Yearn](https://medium.com/@ShapeShift.com/what-is-yearn-shapeshifts-partnership-with-yearn-finance-a94985af1b09)

[Cek workshop strategis dari Facu Ameal pada ETHDenver](https://www.youtube.com/watch?v=6og7NV7lzUk&feature=youtu.be)

[Terimakasih untuk semuanya untuk ETHDenver yang sangat luar biasa](https://twitter.com/iearnfinance/status/1496568330546782208?s=20&t=FA6P4ib_P1NZz_lmoXxvSw)
